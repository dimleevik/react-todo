import React, { Component } from 'react'

import './add-item-form.css'

export default class AddItemForm extends Component {

  state = {
    label: ''
  }

  onChangeLabel = (e) => {
    this.setState({ label: e.target.value })
  }

  onSubmit = (e) => {
    e.preventDefault()
    const { onAdded } = this.props
    onAdded(this.state.label)
    this.setState({ label: '' })
  }

  render() {

    return (
      <form className="add-form" onSubmit={this.onSubmit}>
        <input type='text' onChange={this.onChangeLabel} className="form-control add-input" placeholder="Type todo to add" value={this.state.label}></input>
        <button type="button"
          className="btn btn-outline-primary btn-sm float-right" onClick={this.onSubmit}>
          <i className='fa fa-plus'></i>
        </button>
      </form>
    )
  }

}