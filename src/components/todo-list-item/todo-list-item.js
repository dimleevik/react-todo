import React, { Component } from 'react';

import './todo-list-item.css';

class TodoListItem extends Component {

  render() {
    const { label, onDeleted, onDone, onImportant, done, important } = this.props

    let classNames = `todo-list-item ${done ? 'done ' : ''} ${important ? 'important' : ''}`

    const style = {
      color: important ? 'steelblue' : 'black',
      fontWeight: important ? 'bold' : 'normal'
    };


    return (
      <span className={classNames}>
        <span
          className="todo-list-item-label"
          onClick={onDone}
          style={style}>
          {label}
        </span>
        <div>
          <button type="button"
            className="btn btn-outline-success btn-sm float-right"
            onClick={onImportant}>
            <i className="fa fa-exclamation" />
          </button>

          <button type="button"
            className="btn btn-outline-danger btn-sm float-right"
            onClick={onDeleted}>
            <i className="fa fa-trash" />
          </button>
        </div>

      </span>
    );
  }
}

export default TodoListItem;
