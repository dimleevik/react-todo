import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import AppHeader from './components/app-header';
import SearchPanel from './components/search-panel';
import TodoList from './components/todo-list';
import ItemStatusFilter from './components/item-status-filter';
import AddItemForm from './components/add-item-form';

import './index.css';

export default class App extends Component {
  state = {
    todoData: [
      { label: 'Drink Coffee', important: false, id: 1, done: false },
      { label: 'Make Awesome App', important: true, id: 2, done: false },
      { label: 'Have a lunch', important: false, id: 3, done: false }
    ]
  }

  createElement = (text) => {
    const { todoData } = this.state
    return { label: text, important: false, id: todoData.length + 1, done: false }
  }

  onDeleteItem = (id) => {
    this.setState(({ todoData }) => {
      const data = todoData.filter(({ id: listId }) => listId !== id)
      console.log(data);
      return { todoData: data }
    })
  }

  onAddItem = (text) => {
    this.setState(({ todoData }) => {
      const data = [...todoData, this.createElement(text)]
      return { todoData: data }
    })
  }

  toggleProperty = (id, property) => {
    this.setState(({ todoData }) => {
      const newArray = todoData.map((item) => {
        if (item.id === id) {
          return { ...item, [property]: !item[property] }
        }
        return item
      })
      return { todoData: newArray }
    })
  }

  onToggleDone = (id) => {
    this.toggleProperty(id, 'done')
  }

  onToggleImportant = (id) => {
    this.toggleProperty(id, 'important')
  }

  render() {
    const { todoData } = this.state;
    return (
      <div className="todo-app">
        <AppHeader toDo={todoData.filter(({ done }) => !done).length} done={todoData.filter(({ done }) => done).length} />
        <div className="top-panel d-flex">
          <SearchPanel />
          <ItemStatusFilter />
        </div>

        <TodoList todos={this.state.todoData} onDeleted={this.onDeleteItem} onImportant={this.onToggleImportant} onDone={this.onToggleDone} />
        <AddItemForm onAdded={this.onAddItem} />
      </div>
    );
  }
}

ReactDOM.render(<App />,
  document.getElementById('root'));